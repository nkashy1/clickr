function sendBeacon(event) {
    var xhr = new XMLHttpRequest();
    var tracking_url = "http://localhost:8080/tracking?l=" + encodeURIComponent(location.href) + "&e=" + event;
    xhr.withCredentials = true;
    xhr.open('GET', tracking_url, true);
    console.log("Event: " + event);
    xhr.onreadystatechange = function () {
	if (xhr.readyState == 4 && xhr.status == 200) {
	    var elem = document.getElementById('result');
	    if (elem != null) {
		elem.appendChild(document.createTextNode(xhr.responseText));
	    }
	}
    };
    xhr.send();
    return xhr.responseText;
};


function onBlur() {
    document.body.className = 'blurred';
    sendBeacon("blur");
};
function onFocus(){
   document.body.className = 'focused';
    sendBeacon("focus");
};


//if (/*@cc_on!@*/false) { // check for Internet Explorer
//    document.onfocusin = onFocus;
//    document.onfocusout = onBlur;
//} else {
//    window.onfocus = onFocus;
//    window.onblur = onBlur;
//};

//window.onload = function () { sendBeacon('load'); }
//window.onunload = function () { sendBeacon('unload'); }
