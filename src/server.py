from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site
from twisted.web.static import File

from tracking import TrackingService
from clickr import ClickrService, ClickrPage, ClickrExit
import os

script_dir = os.path.dirname(os.path.abspath(__file__))

root = ClickrService()
root.putChild("tracking", TrackingService())
root.putChild("clickr", ClickrPage())
root.putChild("exit", ClickrExit())
root.putChild("images", File(script_dir + "/../images/"))
root.putChild("js", File(script_dir + "/../js/"))

factory = Site(root)
reactor.listenTCP(8080, factory)
reactor.run()
