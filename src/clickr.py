from twisted.web.resource import Resource
import os
import glob
import random

script_dir = os.path.dirname(os.path.abspath(__file__))

class ClickrService(Resource):

    with open(script_dir + '/../templates/index.html') as startFile:
        startPage = startFile.read()

    def getChild(self, name, request):
        if name == '':
            return self

        return Resource.getChild(self, name, request)
    
    def render_GET(self, request):
        return self.startPage

class ClickrPage(Resource):
    isLeaf = True
    
    with open(script_dir + '/../templates/clickr.html') as clickrFile:
        clickrPage = clickrFile.read()

    imagePath = script_dir + "/../images/*.jpg"
    images = [filename.split('/')[-1] for filename in glob.glob(imagePath)]
    
    def render_GET(self, request):
        image = random.choice(self.images)
        return self.clickrPage.format(image)
        
class ClickrExit(Resource):
    isLeaf = True

    with open(script_dir + '/../templates/exit.html') as exitFile:
        exitPage = exitFile.read()
                
    def render_GET(self, request):
        return self.exitPage
        

        
