import flickrapi
import os
import requests
import sqlite3

key = 'd59ef9d80cc8b9bb4932bafe4d121d10'
secret = '45a69639d1ed9a2b'

flick = flickrapi.FlickrAPI(key, secret)

"""
pullRecent:

This function pulls the 'size' most recent public photos uploaded to Flickr,
where 'size' is an integer between 0 and 500.
"""

def grabRecent():
    responses = map(lambda j: flick.photos.getRecent(per_page = 500, page = j+1, extras = 'tags'), range(2))
    
    photos_meta = []
    for response in responses:
        photos_meta += [photo for photo in response[0]]
    
    return photos_meta


def flickURL(photo_meta):
    URL = 'https://farm{farm}.staticflickr.com/{server}/{id}_{secret}.jpg'.format(**photo_meta.attrib)
    return URL


def flickPic(URL):
    return requests.get(URL).content


def grabNSave(dbfile):
    conn = sqlite3.connect(dbfile)
    c = conn.cursor()
    
    c.execute("CREATE TABLE IF NOT EXISTS photos (ID int PRIMARY KEY, TITLE varchar, URL varchar, OWNER varchar, TAGS varchar)")
    conn.commit()
    
    meta = grabRecent()
    URLs = map(flickURL, meta)
    
    dataset = []
    for j in range(len(meta)):
        attribs = meta[j].attrib
        data = (attribs['id'], attribs['title'], URLs[j], attribs['owner'], attribs['tags'])
        dataset.append(data)
        
        fname = "../images/{id}.jpg".format(id=attribs['id'])
        if not os.path.isfile(fname):
            f = open(fname, 'wb')
            f.write(flickPic(URLs[j]))
            f.close()
    
    c.executemany("INSERT OR IGNORE INTO photos VALUES (?, ?, ?, ?, ?)", dataset)
    conn.commit()
    conn.close()