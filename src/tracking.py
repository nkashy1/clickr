from twisted.web.resource import Resource
from twisted.web.server import Session
from twisted.python.components import registerAdapter
from zope.interface import Interface, Attribute, implements
import json
import datetime, csv, time
import xmlrpclib

class IFrameInfo(Interface):
    u'''Session attribute interface'''
    value = Attribute("Frame Service ID")

class FrameInfo(object):
    u'''Session attribute implementation'''
    implements(IFrameInfo)

    def __init__(self, session):
        u'''Initialize by None'''
        self.value = { "id" : None, "prevTime" : None, "log": [] }

registerAdapter(FrameInfo, Session, IFrameInfo)

class TrackingService(Resource):
    u'''Resource for tracking system'''
    isLeaf = True
    analyzerHost = "localhost"
    analyzerPort = 8357
    frame = xmlrpclib.Server("http://{0}:{1}/".format(analyzerHost,analyzerPort))
    #print("tracking service initialized")
    
    f = file("tracking.csv", "w+")
    writer = csv.writer(f)
    
    def render_GET(self, request):
        dt = datetime.datetime.now()
        session = request.getSession()
        frameInfo = IFrameInfo(session)

        tracking_url = request.args.get('l', ['unknown'])[0]
        tracking_event = request.args.get('e', ['unknown'])[0]

        params = []
        result = []

        self.writer.writerow([session.uid, tracking_url, tracking_event, str(time.mktime(dt.timetuple()) + dt.microsecond/1e6)])
        self.f.flush()
        
        if frameInfo.value['id'] is None:
            newId = self.frame.register()
            self.frame.context(newId)
            frameInfo.value['id'] = newId

        
        if tracking_event == 'exit':
            self.frame.build(frameInfo.value['id'])
            self.frame.switch(frameInfo.value['id'], 'ACTIVE')
            delta = dt - frameInfo.value['prevTime']
            params = self.frame.params(frameInfo.value['id'])
            result = self.frame.impose(frameInfo.value['id'], frameInfo.value['log'], False, True)
            self.frame.goodbye(frameInfo.value['id'])
            session.expire()

        else:
            if frameInfo.value['prevTime'] is not None:
                delta = dt - frameInfo.value['prevTime']
                result = self.frame.impose(frameInfo.value['id'], delta.total_seconds())
                frameInfo.value['log'].append(delta.total_seconds())
                
        #print(result)
        frameInfo.value['prevTime'] = dt
        

        request.setHeader("content-type", "application/json")
        return json.dumps({"rates" : params, "assignments" : result})


