Clickr
======

Requirements
------------

* python 2.x
* [twisted library](https://twistedmatrix.com/trac/)

To install twisted, use pip.

    $ pip install twisted

How to start demo
-----------------

1. start tracking server with following command, `tracking.csv` will be created on current directory.

        $ python src/server.py

1. access to `http://localhost:8080/`
1. move page, ~~and focus or unfocus on page(change tabs or windows)~~

`tracking.csv` columns
----------------------

* session id on twisted server
* URL
* event(load, focus, blur)
* time

